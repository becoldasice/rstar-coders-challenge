## Setting up the project

- Follow the instructions at [TMDb website](https://developers.themoviedb.org/3/getting-started/introduction) to create an API key
- Populate `.env` file with your API Key where indicated

## Running

- Run `yarn start`
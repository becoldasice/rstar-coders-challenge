import React, { useReducer, useCallback } from 'react'
import ReactDOM from 'react-dom'
import WebFont from 'webfontloader'
import * as serviceWorker from './serviceWorker'

import { Container } from '@material-ui/core'

import Header from './components/header'
import Home from './pages/home'

WebFont.load({
  google: {
    families: ['Roboto:300,400,700', 'Material Icons', 'sans-serif'],
  },
})

const initialState = {
  query: undefined,
  rating: undefined,
}

const reducer = (state, action) => {
  switch (action.type) {
    case 'query.change': {
      console.log(action.payload)
      return {
        ...state,
        query: action.payload,
      }
    }
    case 'query.reset': {
      return {
        ...state,
        query: undefined,
      }
    }
    case 'rating.change': {
      return {
        ...state,
        rating: action.payload,
      }
    }
    case 'rating.reset': {
      return { ...state, rating: undefined }
    }
    default: {
      return state
    }
  }
}

const App = () => {
  const [state, dispatch] = useReducer(reducer, initialState)
  const onQueryChange = useCallback((val) => {
    dispatch({ type: 'query.change', payload: val })
  }, [])

  const clearQuery = useCallback(() => {
    dispatch({ type: 'query.reset' })
  }, [])

  const setRating = useCallback((val) => {
    dispatch({ type: 'rating.change', payload: val })
  }, [])

  const clearRating = useCallback(() => {
    dispatch({ type: 'rating.reset' })
  }, [])
  return (
    <React.Fragment>
      <Header onQueryChange={onQueryChange} clearQuery={clearQuery} />
      <Container maxWidth="lg">
        <Home
          rating={state.rating}
          query={state.query}
          setRating={setRating}
          clearRating={clearRating}
        />
      </Container>
    </React.Fragment>
  )
}

ReactDOM.render(<App />, document.getElementById('root'))

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister()

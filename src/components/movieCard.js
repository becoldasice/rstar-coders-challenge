import React from 'react'
import {
  Card,
  CardActionArea,
  CardMedia,
  CardContent,
  Typography,
} from '@material-ui/core'
import PropTypes from 'prop-types'
import { makeStyles } from '@material-ui/core/styles'

const POSTER_URL = `${process.env.REACT_APP_MOVIE_POSTER_BASE_URL}w500`

const useStyles = makeStyles((theme) => ({
  mediaSize: {
    height: '420px',
    backgroundColor: theme.palette.grey[500],
  },
}))

const MovieCard = ({ title, id, posterPath, onClick }) => {
  const classes = useStyles()

  return (
    <Card onClick={onClick}>
      <CardActionArea>
        <CardMedia
          image={`${POSTER_URL}${posterPath}`}
          className={classes.mediaSize}
        />
        <CardContent>
          <Typography>{title}</Typography>
        </CardContent>
      </CardActionArea>
    </Card>
  )
}

MovieCard.propTypes = {
  title: PropTypes.string.isRequired,
  posterPath: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired,
  id: PropTypes.number,
}

export default MovieCard

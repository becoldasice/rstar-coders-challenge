import React from 'react'
import PropTypes from 'prop-types'
import { makeStyles } from '@material-ui/core/styles'
import {
  Button,
  Card,
  CardHeader,
  CardMedia,
  CardContent,
  CardActions,
  Dialog,
  Grid,
  Zoom,
  Typography,
} from '@material-ui/core'

const POSTER_URL = `${process.env.REACT_APP_MOVIE_POSTER_BASE_URL}w500`

const useStyles = makeStyles((theme) => ({
  container: {
    padding: '16px',
  },
  imageContainer: {
    display: 'flex',
    padding: '24px !important',
  },
  image: {
    flex: 1,
    backgroundSize: 'cover',
    height: '513px',
    backgroundColor: theme.palette.grey[500],
  },
  contentContainer: {
    display: 'flex',
    flexFlow: 'column',
  },
  actionsContainer: {
    justifyContent: 'flex-end',
    marginTop: 'auto',
  },
}))

const DetailsModal = ({ open, closeModal, movie }) => {
  const classes = useStyles()
  const body = () => {
    if (!movie) return null
    return (
      <Grid container spacing={4} className={classes.container}>
        <Grid item sm={5} className={classes.imageContainer}>
          <CardMedia
            image={`${POSTER_URL}${movie.posterPath}`}
            className={classes.image}
          />
        </Grid>
        <Grid item sm={7} className={classes.contentContainer}>
          <CardHeader title={movie.title} />
          <CardContent>
            <Typography>
              <b>Rating:</b> {movie.rating}/10
            </Typography>
            <Typography>
              <b>Overview:</b> {movie.overview}
            </Typography>
          </CardContent>
          <CardActions className={classes.actionsContainer}>
            <Button size="small" onClick={closeModal} color="primary">
              Close
            </Button>
          </CardActions>
        </Grid>
      </Grid>
    )
  }
  return (
    <Dialog
      PaperComponent={Card}
      PaperProps={{
        variant: 'outlined',
      }}
      open={open}
      maxWidth="md"
      onBackdropClick={closeModal}
      onEscapeKeyDown={closeModal}
      TransitionComponent={Zoom}
    >
      {body()}
    </Dialog>
  )
}

DetailsModal.defaultProps = {
  open: false,
}

DetailsModal.propTypes = {
  open: PropTypes.bool,
  closeModal: PropTypes.func.isRequired,
  movie: PropTypes.shape({
    title: PropTypes.string.isRequired,
    overview: PropTypes.string.isRequired,
    rating: PropTypes.number.isRequired,
    posterPath: PropTypes.string,
  }),
}

export default DetailsModal

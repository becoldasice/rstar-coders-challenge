import React from 'react'
import PropTypes from 'prop-types'
import { Paper, Typography, Grid } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'
import Rating from '@material-ui/lab/Rating'
import CloseIcon from '@material-ui/icons/Close'
import StarBorderIcon from '@material-ui/icons/StarBorder'

const useStyles = makeStyles((theme) => ({
  container: {
    maxWidth: 255,
    backgroundColor: theme.palette.primary.main,
    color: 'white',
    borderRadius: '50px',
    paddingLeft: '16px',
  },
  gridItem: {
    flex: 'auto',
    height: '32px',
  },
  emptyRating: {
    color: 'white',
  },
  closeIcon: {
    cursor: 'pointer',
  },
}))

const Filters = ({ rating, setRating, clearRating }) => {
  const classes = useStyles()
  const actualRating = Math.ceil(rating / 2)
  return (
    <Grid
      component={Paper}
      elevation={0}
      container
      spacing={1}
      justify="space-between"
      alignItems="center"
      className={classes.container}
    >
      <Grid item className={classes.gridItem}>
        <Typography component="legend">Rating: </Typography>
      </Grid>
      <Grid item className={classes.gridItem}>
        <Rating
          value={actualRating}
          onChange={(e, val) => setRating(val * 2)}
          emptyIcon={<StarBorderIcon className={classes.emptyRating} />}
          name="movies-rating"
        />
      </Grid>
      <Grid item className={classes.gridItem}>
        <CloseIcon onClick={clearRating} className={classes.closeIcon} />
      </Grid>
    </Grid>
  )
}

Filters.propTypes = {
  rating: PropTypes.number,
  setRating: PropTypes.func.isRequired,
  clearRating: PropTypes.func.isRequired,
}

export default Filters

import React, { useState, useEffect } from 'react'
import PropTypes from 'prop-types'
import { AppBar, Toolbar, Typography, InputBase, Grid } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'
import SearchIcon from '@material-ui/icons/Search'
import CloseIcon from '@material-ui/icons/Close'

const useStyles = makeStyles({
  searchBarContainer: {
    alignItems: 'center',
    backgroundColor: 'white',
    borderRadius: '50px',
    display: 'flex',
    marginLeft: 'auto',
    maxWidth: 400,
    width: '40%',
  },
  input: {
    flex: '1',
    margin: '0 8px',
  },
  closeIcon: {
    cursor: 'pointer',
  },
  toolbar: {
    paddingTop: '8px',
    paddingBottom: '8px',
  },
})

const Header = ({ query, onQueryChange, clearQuery }) => {
  const classes = useStyles()
  const [localQuery, setLocalQuery] = useState('')

  const changeLocalQuery = (e) => {
    setLocalQuery(e.target.value)
  }

  const clearLocalQuery = () => {
    setLocalQuery('')
  }

  useEffect(() => {
    if (localQuery.trim() !== '') {
      const timeout = setTimeout(() => {
        onQueryChange(localQuery)
      }, 300)
      return () => clearTimeout(timeout)
    } else {
      clearQuery()
    }
  }, [localQuery, onQueryChange, clearQuery])

  return (
    <React.Fragment>
      <AppBar>
        <Toolbar className={classes.toolbar}>
          <Grid container spacing={2} alignItems="center">
            <Grid item>
              <Typography variant="h6">FindYourMovie</Typography>
            </Grid>
            <Grid item className={classes.searchBarContainer}>
              <SearchIcon color="primary" />
              <InputBase
                placeholder="Search for your movie"
                onChange={changeLocalQuery}
                className={classes.input}
                value={localQuery}
              />
              <CloseIcon
                onClick={clearLocalQuery}
                color="primary"
                className={classes.closeIcon}
              />
            </Grid>
          </Grid>
        </Toolbar>
      </AppBar>
      <Toolbar className={classes.toolbar} />
    </React.Fragment>
  )
}

Header.propTypes = {
  onQueryChange: PropTypes.func.isRequired,
  clearQuery: PropTypes.func.isRequired,
  query: PropTypes.string,
}

export default Header

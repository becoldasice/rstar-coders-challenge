const API_KEY = process.env.REACT_APP_MOVIE_DB_API_KEY
const BASE_URL = (path) =>
  `https://api.themoviedb.org/3${path}?api_key=${API_KEY}&include_adult=false&`

const performFetch = async (url) => {
  const response = await fetch(url)
  const data = await response.json()
  return data
}
const fetchMovies = async (average = 10) =>
  await performFetch(
    `${BASE_URL('/discover/movie')}vote_average.lte=${average}`
  )

const searchMovies = async (query) =>
  await performFetch(`${BASE_URL('/search/movie')}query=${query}`)

export { fetchMovies, searchMovies }

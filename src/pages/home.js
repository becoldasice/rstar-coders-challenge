import React, { useEffect, useState } from 'react'
import PropTypes from 'prop-types'
import { Typography, Grid, CircularProgress } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'

import MovieCard from 'components/movieCard'
import Filters from 'components/filters'
import DetailsModal from 'components/detailsModal'

import { fetchMovies, searchMovies } from 'api'

const useStyles = makeStyles({
  titleContainer: {
    padding: '24px 8px',
  },
  loader: {
    margin: '40px auto 0 auto',
  },
})

const Home = ({ query, rating, setRating, clearRating }) => {
  const classes = useStyles()
  const [movies, setMovies] = useState([])
  const [loading, setLoading] = useState(true)
  const [detailedMovie, setDetailedMovie] = useState(undefined)

  useEffect(() => {
    async function loadMovies() {
      const movies =
        query === undefined
          ? await fetchMovies(rating)
          : await searchMovies(query)
      setMovies(movies.results)
      setLoading(false)
    }
    setLoading(true)
    loadMovies()
  }, [query, rating])

  const renderMovies = () => {
    if (movies.length === 0)
      return (
        <Grid item>
          <Typography>No results found for your search</Typography>
        </Grid>
      )
    return movies.map((m) => (
      <Grid item xs={6} sm={4} md={3} key={m.id}>
        <MovieCard
          id={m.id}
          title={m.title}
          posterPath={m.poster_path}
          onClick={() =>
            setDetailedMovie({
              title: m.title,
              overview: m.overview,
              rating: m.vote_average,
              posterPath: m.poster_path,
            })
          }
        />
      </Grid>
    ))
  }

  return (
    <Grid container spacing={2}>
      <Grid
        container
        justify="space-between"
        className={classes.titleContainer}
      >
        <Typography variant="h5" component="h1">
          {query === undefined
            ? 'Most Popular Movies'
            : `Search results for: "${query}"`}
        </Typography>
        {query === undefined ? (
          <Filters
            rating={rating}
            setRating={setRating}
            clearRating={clearRating}
          />
        ) : null}
      </Grid>
      {loading ? (
        <CircularProgress size={80} className={classes.loader} />
      ) : (
        renderMovies()
      )}
      <DetailsModal
        open={detailedMovie !== undefined}
        closeModal={() => setDetailedMovie(undefined)}
        movie={detailedMovie}
      />
    </Grid>
  )
}

Home.propTypes = {
  query: PropTypes.string,
  rating: PropTypes.number,
  setRating: PropTypes.func.isRequired,
  clearRating: PropTypes.func.isRequired,
}

export default Home
